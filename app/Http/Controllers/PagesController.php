<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
  public function home()
  {

    $people = ['Taylor', 'Matt', 'Jeffrey'];
    return view('pages/home', compact('people'));
  }  

  public function about()
  {
    return view('pages/about');
  }

  public function contact()
  {
    return view('pages/contact');
  }
}
