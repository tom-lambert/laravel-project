<?php

namespace App\Http\Controllers;

use App\Note;
use App\Card;
use Illuminate\Http\Request;

class NotesController extends Controller
{
  public function store(Request $request, Card $card)
  {
    /* $note = new Note; */
    /* $note->body = $request->body; */
    /* $card->notes()->save($note); */

    $card->notes()->create([
      'body' => $request->body
    ]); 
    /* $card->addNote( */
    /*   new Note(['body' => $request->body]) */
    /* ); */

    return back();

  }

  public function edit(Note $note)
  {
    return view('notes.edit', compact('note'));
  }

  public function update(Request $request, Note $note)
  {
    $params = $request->only('body');
    $note->upate($params);

    return back();
  }
}
