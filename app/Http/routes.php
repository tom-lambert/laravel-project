<?php

Route::get('about', 'PagesController@about');

Route::get('/', 'PagesController@home');

Route::get('contact', 'PagesController@contact');

Route::get('cards', 'CardsController@index');
Route::get('cards/{card}', 'CardsController@show');

Route::post('cards/{card}/notes', 'NotesController@store');

Route::get('/notes/{note}/edit', 'NotesController@edit');
Route::patch('notes/{note}', 'NotesController@update');
