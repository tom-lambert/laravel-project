<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project</title>
  <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>
<body>
  <div class="container">

    @yield('content')

  </div>

  <script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
